import React, { useEffect } from "react";
import SplashScreen from "react-native-splash-screen";
import AppNavigator from "./src/navigation/AppNavigator";
import { StatusBar, DeviceEventEmitter } from "react-native";

const preLoad = async () => {
  SplashScreen.hide();
};

const App = () => {
  useEffect(() => {
    preLoad();
  }, []);

  return (
    <>
      <StatusBar backgroundColor="black" barStyle="light-content" />
      <AppNavigator
        onNavigationStateChange={(prevState, currentState) => {
          let route = currentState;
          while (route.routes) {
            route = route.routes[route.index];
          }
          DeviceEventEmitter.emit("routeStateChanged", route);
        }}
      />
    </>
  );
};

export default App;
