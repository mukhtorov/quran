import React, { Component } from "react";
import { InMemoryCache } from "apollo-cache-inmemory";
import ApolloClient from "apollo-boost";
import AsyncStorage from "@react-native-community/async-storage";

const initialState = {
  general: {
    backgroundColor: "#545454"
  },
  night: {
    backgroundColor: "#545454",
    color: "white"
  },
  day: {
    backgroundColor: "white",
    color: "black"
  }
};
const QuranContext = React.createContext(initialState);

interface Props {
  navigation: any;
}
class QuranProvider extends Component<Props, any> {
  render() {
    return (
      <QuranContext.Provider value={initialState}>
        {this.props.children}
      </QuranContext.Provider>
    );
  }
}

export default QuranContext;
