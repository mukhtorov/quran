import React from "react";
import Signup from "../components/Signup";

const SettingsScreen = () => {
  return <Signup />;
};

SettingsScreen.navigationOptions = {
  title: "Settings"
};

export default SettingsScreen;
