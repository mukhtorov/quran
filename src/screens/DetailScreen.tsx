import React, { useEffect } from "react";
import Detail from "../components/Detail";
import Test from "../components/testing";
import { withNavigation } from "react-navigation";

var title = "";
const DetailScreen = ({ navigation }) => {
  console.log(navigation.state.params);
  useEffect(() => {
    title = navigation.state.params.title;
  });
  // return <Test />;
  return <Detail />;
};

DetailScreen.navigationOptions = ({ navigation }) => ({
  title: `${navigation.state.params.title}`
});

export default withNavigation(DetailScreen);
