import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import Facebook from "../components/Facebook";

const FacebookScreen = () => {
  return <Facebook />;
};

FacebookScreen.navigationOptions = {
  title: "Facebook'",
  tabBarIcon: ({ tintColor }) => (
    <Icon name="facebook" size={24} color={tintColor} />
  )
};

export default FacebookScreen;
