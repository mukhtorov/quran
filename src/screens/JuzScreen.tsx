import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import Juz from "../components/Juz";

const JuzScreen = () => {
  return <Juz />;
};

JuzScreen.navigationOptions = {
  title: "Juz'",
  tabBarIcon: ({ tintColor }) => (
    <Icon name="facebook" size={24} color={tintColor} />
  )
};

export default JuzScreen;
