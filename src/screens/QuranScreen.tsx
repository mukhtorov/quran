import React from "react";
import Icon from "react-native-vector-icons/Feather";
import Quran from "../components/Quran";

const QuranScreen = () => {
  return <Quran />;
};

QuranScreen.navigationOptions = {
  title: "Surah",
  tabBarIcon: ({ tintColor }) => (
    <Icon name="book-open" size={24} color={tintColor} />
  )
};

export default QuranScreen;
