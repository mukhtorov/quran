import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import Bookmark from "../components/Bookmark";

const BookmarkScreen = () => {
  return <Bookmark />;
};

BookmarkScreen.navigationOptions = {
  title: "Bookmarks",
  tabBarIcon: ({ tintColor }) => (
    <Icon name="bookmark" size={24} color={tintColor} />
  )
};

export default BookmarkScreen;
