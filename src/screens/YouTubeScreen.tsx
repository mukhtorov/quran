import React from "react";
import YouTube from "../components/YouTube";
import Icon from "react-native-vector-icons/FontAwesome";
import { Platform } from "react-native";

const YouTubeScreen = () => {
  return <YouTube />;
};

// YouTubeScreen.navigationOptions = {
//   title: "YouTube"
// };
YouTubeScreen.navigationOptions = {
  title: "YouTube",
  tabBarIcon: ({ tintColor, focused }) => (
    <Icon
      // name={Platform.OS === "ios" ? `youtube` : "youtube"}
      name="youtube"
      size={24}
      color={tintColor}
    />
  )
};
export default YouTubeScreen;
