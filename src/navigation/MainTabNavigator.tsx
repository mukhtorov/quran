import BookmarkScreen from "../screens/BookmarkScreen";
import QuranScreen from "../screens/QuranScreen";
import FacebookScreen from "../screens/FacebookScreen";
import JusScreen from "../screens/JuzScreen";
import YouTubeScreen from "../screens/YouTubeScreen";
import styled from "styled-components";
import React, { useEffect, useState } from "react";
import { InteractionManager } from "react-native";
import ScrollableTabView from "react-native-scrollable-tab-view";
import TabBar from "../components/shared/TabBar";

const StyledActivityIndicator = styled.ActivityIndicator`
  flex: 1;
  background-color: #6c8edb;
`;

const TabView = ({ component, tabLabel }) => {
  const [didFinishInitialAnimation, setDidFinishInitialAnimation] = useState(
    false
  );

  useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
      setDidFinishInitialAnimation(true);
    });
  }, []);

  if (didFinishInitialAnimation) {
    return component;
  } else {
    return <StyledActivityIndicator />;
  }
};

const MainTabNavigator = (props, { isOpened }) => {
  return (
    <ScrollableTabView
      scrollWithoutAnimation={true}
      locked={true}
      tabBarPosition="bottom"
      renderTabBar={() => <TabBar />}
    >
      <TabView tabLabel="book" component={<QuranScreen />} />
      <TabView tabLabel="th-list" component={<JusScreen />} />
      <TabView tabLabel="bookmark" component={<BookmarkScreen />} />
      <TabView tabLabel="facebook" component={<FacebookScreen />} />
      <TabView tabLabel="youtube" component={<YouTubeScreen />} />
    </ScrollableTabView>
  );
};

export default MainTabNavigator;
