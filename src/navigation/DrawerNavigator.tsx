import { Dimensions } from "react-native";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createAppContainer } from "react-navigation";
import MainNavigator from "./MainNavigator";
import Drawer from "../components/Drawer";

const WIDTH = Dimensions.get("window").width;

const DrawerNavigator = createDrawerNavigator(
  {
    Main: { screen: MainNavigator }
  },
  {
    initialRouteName: "Main",
    contentComponent: Drawer,
    drawerPosition: "right",
    drawerWidth: WIDTH * 0.85,
    drawerBackgroundColor: "#2e2e2e",
    drawerType: "slide"
  }
);

export default createAppContainer(DrawerNavigator);
