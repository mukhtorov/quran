import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import MainTabNavigator from "./MainTabNavigator";
import HeaderTitle from "../components/shared/HeaderTitle";
import HeaderRight from "../components/shared/HeaderRight";
import SignupScreen from "../screens/SettingsScreen";
import DetailScreen from "../screens/DetailScreen";
import ProfileViewScreen from "../screens/YouTubeScreen";

const MainNavigator = createStackNavigator(
  {
    MainTab: {
      screen: MainTabNavigator,
      navigationOptions: {
        headerTitle: <HeaderTitle />,
        headerRight: <HeaderRight />
      }
    },
    Signup: { screen: SignupScreen },
    Detail: { screen: DetailScreen },
    ProfileView: { screen: ProfileViewScreen }
  },
  {
    initialRouteName: "MainTab",
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: {
        // marginTop: 55,
        backgroundColor: "red"
      },
      headerTintColor: "#6c8edb"
    })
  }
);

export default MainNavigator;
