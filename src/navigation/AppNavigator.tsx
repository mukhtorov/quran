import { createAppContainer, createSwitchNavigator } from "react-navigation";

import DrawerNavigator from "./DrawerNavigator";

const AppNavigator = createAppContainer(
  createSwitchNavigator({
    Drawer: { screen: DrawerNavigator }
  })
);

export default AppNavigator;
