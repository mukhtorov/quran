export default {
  name: "al-Ashr",
  index: "103",
  verse: [
    "In the name of God, the Gracious, the Merciful.",
    "By time.",
    "The human being is in loss.",
    "Except those who believe, and do good works, and encourage truth, and recommend patience."
  ],
  count: 3
};
