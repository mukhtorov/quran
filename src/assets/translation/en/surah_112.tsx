export default {
  name: "al-Ikhlash",
  index: "112",
  verse: [
    "In the name of God, the Gracious, the Merciful.",
    'Say, "He is God, the One.',
    "God, the Absolute.",
    "He begets not, nor was He begotten.",
    'And there is nothing comparable to Him."'
  ],
  count: 4
};
