import surah1 from "./translation/en/surah_1";
import surah2 from "./translation/en/surah_2";
import surah3 from "./translation/en/surah_3";
import surah4 from "./translation/en/surah_4";
import surah5 from "./translation/en/surah_5";
import surah6 from "./translation/en/surah_6";
import surah7 from "./translation/en/surah_7";
import surah8 from "./translation/en/surah_8";
import surah9 from "./translation/en/surah_9";
import surah10 from "./translation/en/surah_10";
import surah11 from "./translation/en/surah_11";
import surah12 from "./translation/en/surah_12";
import surah13 from "./translation/en/surah_13";
import surah14 from "./translation/en/surah_14";
import surah15 from "./translation/en/surah_15";
import surah16 from "./translation/en/surah_16";
import surah17 from "./translation/en/surah_17";
import surah18 from "./translation/en/surah_18";
import surah19 from "./translation/en/surah_19";
import surah20 from "./translation/en/surah_20";
import surah21 from "./translation/en/surah_21";
import surah22 from "./translation/en/surah_22";
import surah23 from "./translation/en/surah_23";
import surah24 from "./translation/en/surah_24";
import surah25 from "./translation/en/surah_25";
import surah26 from "./translation/en/surah_26";
import surah27 from "./translation/en/surah_27";
import surah28 from "./translation/en/surah_28";
import surah29 from "./translation/en/surah_29";
import surah30 from "./translation/en/surah_30";
import surah31 from "./translation/en/surah_31";
import surah32 from "./translation/en/surah_32";
import surah33 from "./translation/en/surah_33";
import surah34 from "./translation/en/surah_34";
import surah35 from "./translation/en/surah_35";
import surah36 from "./translation/en/surah_36";
import surah37 from "./translation/en/surah_37";
import surah38 from "./translation/en/surah_38";
import surah39 from "./translation/en/surah_39";
import surah40 from "./translation/en/surah_40";
import surah41 from "./translation/en/surah_41";
import surah42 from "./translation/en/surah_42";
import surah43 from "./translation/en/surah_43";
import surah44 from "./translation/en/surah_44";
import surah45 from "./translation/en/surah_45";
import surah46 from "./translation/en/surah_46";
import surah47 from "./translation/en/surah_47";
import surah48 from "./translation/en/surah_48";
import surah49 from "./translation/en/surah_49";
import surah50 from "./translation/en/surah_50";
import surah51 from "./translation/en/surah_51";
import surah52 from "./translation/en/surah_52";
import surah53 from "./translation/en/surah_53";
import surah54 from "./translation/en/surah_54";
import surah55 from "./translation/en/surah_55";
import surah56 from "./translation/en/surah_56";
import surah57 from "./translation/en/surah_57";
import surah58 from "./translation/en/surah_58";
import surah59 from "./translation/en/surah_59";
import surah60 from "./translation/en/surah_60";
import surah61 from "./translation/en/surah_61";
import surah62 from "./translation/en/surah_62";
import surah63 from "./translation/en/surah_63";
import surah64 from "./translation/en/surah_64";
import surah65 from "./translation/en/surah_65";
import surah66 from "./translation/en/surah_66";
import surah67 from "./translation/en/surah_67";
import surah68 from "./translation/en/surah_68";
import surah69 from "./translation/en/surah_69";
import surah70 from "./translation/en/surah_70";
import surah71 from "./translation/en/surah_71";
import surah72 from "./translation/en/surah_72";
import surah73 from "./translation/en/surah_73";
import surah74 from "./translation/en/surah_74";
import surah75 from "./translation/en/surah_75";
import surah76 from "./translation/en/surah_76";
import surah77 from "./translation/en/surah_77";
import surah78 from "./translation/en/surah_78";
import surah79 from "./translation/en/surah_79";
import surah80 from "./translation/en/surah_80";
import surah81 from "./translation/en/surah_81";
import surah82 from "./translation/en/surah_82";
import surah83 from "./translation/en/surah_83";
import surah84 from "./translation/en/surah_84";
import surah85 from "./translation/en/surah_85";
import surah86 from "./translation/en/surah_86";
import surah87 from "./translation/en/surah_87";
import surah88 from "./translation/en/surah_88";
import surah89 from "./translation/en/surah_89";
import surah90 from "./translation/en/surah_90";
import surah91 from "./translation/en/surah_91";
import surah92 from "./translation/en/surah_92";
import surah93 from "./translation/en/surah_93";
import surah94 from "./translation/en/surah_94";
import surah95 from "./translation/en/surah_95";
import surah96 from "./translation/en/surah_96";
import surah97 from "./translation/en/surah_97";
import surah98 from "./translation/en/surah_98";
import surah99 from "./translation/en/surah_99";
import surah100 from "./translation/en/surah_100";
import surah101 from "./translation/en/surah_101";
import surah102 from "./translation/en/surah_102";
import surah103 from "./translation/en/surah_103";
import surah104 from "./translation/en/surah_104";
import surah105 from "./translation/en/surah_105";
import surah106 from "./translation/en/surah_106";
import surah107 from "./translation/en/surah_107";
import surah108 from "./translation/en/surah_108";
import surah109 from "./translation/en/surah_109";
import surah110 from "./translation/en/surah_110";
import surah111 from "./translation/en/surah_111";
import surah112 from "./translation/en/surah_112";
import surah113 from "./translation/en/surah_113";
import surah114 from "./translation/en/surah_114";

export default {
  surah1,
  surah2,
  surah3,
  surah4,
  surah5,
  surah6,
  surah7,
  surah8,
  surah9,
  surah10,
  surah11,
  surah12,
  surah13,
  surah14,
  surah15,
  surah16,
  surah17,
  surah18,
  surah19,
  surah20,
  surah21,
  surah22,
  surah23,
  surah24,
  surah25,
  surah26,
  surah27,
  surah28,
  surah29,
  surah30,
  surah31,
  surah32,
  surah33,
  surah34,
  surah35,
  surah36,
  surah37,
  surah38,
  surah39,
  surah40,
  surah41,
  surah42,
  surah43,
  surah44,
  surah45,
  surah46,
  surah47,
  surah48,
  surah49,
  surah50,
  surah51,
  surah52,
  surah53,
  surah54,
  surah55,
  surah56,
  surah57,
  surah58,
  surah59,
  surah60,
  surah61,
  surah62,
  surah63,
  surah64,
  surah65,
  surah66,
  surah67,
  surah68,
  surah69,
  surah70,
  surah71,
  surah72,
  surah73,
  surah74,
  surah75,
  surah76,
  surah77,
  surah78,
  surah79,
  surah80,
  surah81,
  surah82,
  surah83,
  surah84,
  surah85,
  surah86,
  surah87,
  surah88,
  surah89,
  surah90,
  surah91,
  surah92,
  surah93,
  surah94,
  surah95,
  surah96,
  surah97,
  surah98,
  surah99,
  surah100,
  surah101,
  surah102,
  surah103,
  surah104,
  surah105,
  surah106,
  surah107,
  surah108,
  surah109,
  surah110,
  surah111,
  surah112,
  surah113,
  surah114
};
