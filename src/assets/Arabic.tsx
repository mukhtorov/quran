import surah1 from "./surah/surah_1";
import surah2 from "./surah/surah_2";
import surah3 from "./surah/surah_3";
import surah4 from "./surah/surah_4";
import surah5 from "./surah/surah_5";
import surah6 from "./surah/surah_6";
import surah7 from "./surah/surah_7";
import surah8 from "./surah/surah_8";
import surah9 from "./surah/surah_9";
import surah10 from "./surah/surah_10";
import surah11 from "./surah/surah_11";
import surah12 from "./surah/surah_12";
import surah13 from "./surah/surah_13";
import surah14 from "./surah/surah_14";
import surah15 from "./surah/surah_15";
import surah16 from "./surah/surah_16";
import surah17 from "./surah/surah_17";
import surah18 from "./surah/surah_18";
import surah19 from "./surah/surah_19";
import surah20 from "./surah/surah_20";
import surah21 from "./surah/surah_21";
import surah22 from "./surah/surah_22";
import surah23 from "./surah/surah_23";
import surah24 from "./surah/surah_24";
import surah25 from "./surah/surah_25";
import surah26 from "./surah/surah_26";
import surah27 from "./surah/surah_27";
import surah28 from "./surah/surah_28";
import surah29 from "./surah/surah_29";
import surah30 from "./surah/surah_30";
import surah31 from "./surah/surah_31";
import surah32 from "./surah/surah_32";
import surah33 from "./surah/surah_33";
import surah34 from "./surah/surah_34";
import surah35 from "./surah/surah_35";
import surah36 from "./surah/surah_36";
import surah37 from "./surah/surah_37";
import surah38 from "./surah/surah_38";
import surah39 from "./surah/surah_39";
import surah40 from "./surah/surah_40";
import surah41 from "./surah/surah_41";
import surah42 from "./surah/surah_42";
import surah43 from "./surah/surah_43";
import surah44 from "./surah/surah_44";
import surah45 from "./surah/surah_45";
import surah46 from "./surah/surah_46";
import surah47 from "./surah/surah_47";
import surah48 from "./surah/surah_48";
import surah49 from "./surah/surah_49";
import surah50 from "./surah/surah_50";
import surah51 from "./surah/surah_51";
import surah52 from "./surah/surah_52";
import surah53 from "./surah/surah_53";
import surah54 from "./surah/surah_54";
import surah55 from "./surah/surah_55";
import surah56 from "./surah/surah_56";
import surah57 from "./surah/surah_57";
import surah58 from "./surah/surah_58";
import surah59 from "./surah/surah_59";
import surah60 from "./surah/surah_60";
import surah61 from "./surah/surah_61";
import surah62 from "./surah/surah_62";
import surah63 from "./surah/surah_63";
import surah64 from "./surah/surah_64";
import surah65 from "./surah/surah_65";
import surah66 from "./surah/surah_66";
import surah67 from "./surah/surah_67";
import surah68 from "./surah/surah_68";
import surah69 from "./surah/surah_69";
import surah70 from "./surah/surah_70";
import surah71 from "./surah/surah_71";
import surah72 from "./surah/surah_72";
import surah73 from "./surah/surah_73";
import surah74 from "./surah/surah_74";
import surah75 from "./surah/surah_75";
import surah76 from "./surah/surah_76";
import surah77 from "./surah/surah_77";
import surah78 from "./surah/surah_78";
import surah79 from "./surah/surah_79";
import surah80 from "./surah/surah_80";
import surah81 from "./surah/surah_81";
import surah82 from "./surah/surah_82";
import surah83 from "./surah/surah_83";
import surah84 from "./surah/surah_84";
import surah85 from "./surah/surah_85";
import surah86 from "./surah/surah_86";
import surah87 from "./surah/surah_87";
import surah88 from "./surah/surah_88";
import surah89 from "./surah/surah_89";
import surah90 from "./surah/surah_90";
import surah91 from "./surah/surah_91";
import surah92 from "./surah/surah_92";
import surah93 from "./surah/surah_93";
import surah94 from "./surah/surah_94";
import surah95 from "./surah/surah_95";
import surah96 from "./surah/surah_96";
import surah97 from "./surah/surah_97";
import surah98 from "./surah/surah_98";
import surah99 from "./surah/surah_99";
import surah100 from "./surah/surah_100";
import surah101 from "./surah/surah_101";
import surah102 from "./surah/surah_102";
import surah103 from "./surah/surah_103";
import surah104 from "./surah/surah_104";
import surah105 from "./surah/surah_105";
import surah106 from "./surah/surah_106";
import surah107 from "./surah/surah_107";
import surah108 from "./surah/surah_108";
import surah109 from "./surah/surah_109";
import surah110 from "./surah/surah_110";
import surah111 from "./surah/surah_111";
import surah112 from "./surah/surah_112";
import surah113 from "./surah/surah_113";
import surah114 from "./surah/surah_114";

export default {
  surah1,
  surah2,
  surah3,
  surah4,
  surah5,
  surah6,
  surah7,
  surah8,
  surah9,
  surah10,
  surah11,
  surah12,
  surah13,
  surah14,
  surah15,
  surah16,
  surah17,
  surah18,
  surah19,
  surah20,
  surah21,
  surah22,
  surah23,
  surah24,
  surah25,
  surah26,
  surah27,
  surah28,
  surah29,
  surah30,
  surah31,
  surah32,
  surah33,
  surah34,
  surah35,
  surah36,
  surah37,
  surah38,
  surah39,
  surah40,
  surah41,
  surah42,
  surah43,
  surah44,
  surah45,
  surah46,
  surah47,
  surah48,
  surah49,
  surah50,
  surah51,
  surah52,
  surah53,
  surah54,
  surah55,
  surah56,
  surah57,
  surah58,
  surah59,
  surah60,
  surah61,
  surah62,
  surah63,
  surah64,
  surah65,
  surah66,
  surah67,
  surah68,
  surah69,
  surah70,
  surah71,
  surah72,
  surah73,
  surah74,
  surah75,
  surah76,
  surah77,
  surah78,
  surah79,
  surah80,
  surah81,
  surah82,
  surah83,
  surah84,
  surah85,
  surah86,
  surah87,
  surah88,
  surah89,
  surah90,
  surah91,
  surah92,
  surah93,
  surah94,
  surah95,
  surah96,
  surah97,
  surah98,
  surah99,
  surah100,
  surah101,
  surah102,
  surah103,
  surah104,
  surah105,
  surah106,
  surah107,
  surah108,
  surah109,
  surah110,
  surah111,
  surah112,
  surah113,
  surah114
};
