import React, { useState, useEffect, Component } from "react";
import {
  TouchableOpacity,
  Image,
  FlatList,
  Text,
  Dimensions,
  View
} from "react-native";
import { withNavigation } from "react-navigation";
import { BodyContainer, Container, Icons, Title } from "./styles/DetailStyle";
import QuranContext from "../contexts/QuranProvider";
import Arabic from "../assets/Arabic";
import English from "../assets/English";
import {
  RecyclerListView,
  DataProvider,
  LayoutProvider
} from "recyclerlistview";

interface Props {
  navigation: any;
}
export default class RecycleTestComponent extends React.Component<Props, any> {
  constructor(props) {
    super(props);
    console.log("stat", props);
    let { width } = Dimensions.get("window");

    //Create the data provider and provide method which takes in two rows of data and return if those two are different or not.
    let dataProvider = new DataProvider((r1, r2) => {
      return r1 !== r2;
    });

    //Since component should always render once data has changed, make data provider part of the state
    this.state = {
      dataProvider: dataProvider.cloneWithRows(Arabic.surah2.verse),
      layoutProvider: new LayoutProvider(
        index => {
          return 0;
        },
        (type, dim) => {
          dim.width = width;
          dim.height = 100;
        }
      )
    };
  }

  //Given type and data return the view component
  rowRenderer = (type, data, index) => {
    return (
      <View>
        <Text>
          Arabic: {index} .{data}
        </Text>
        <Text>English: {English["surah2"].verse[index + 1]}</Text>
      </View>
    );
  };

  render() {
    return (
      <RecyclerListView
        layoutProvider={this.state.layoutProvider}
        dataProvider={this.state.dataProvider}
        rowRenderer={this.rowRenderer}
      />
    );
  }
}
