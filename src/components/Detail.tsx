import React, { Component } from "react";
import { TouchableOpacity, Dimensions } from "react-native";
import { withNavigation } from "react-navigation";
import { BodyContainer, Container, Icons, Title } from "./styles/DetailStyle";
import QuranContext from "../contexts/QuranProvider";
import Arabic from "../assets/Arabic";
import English from "../assets/English";
import {
  RecyclerListView,
  DataProvider,
  LayoutProvider
} from "recyclerlistview";

let { width, height } = Dimensions.get("window");

interface Props {
  navigation: any;
}

class Details extends Component<Props, any> {
  constructor(props) {
    super(props);

    const arabicVerses =
      Arabic[this.props.navigation.state.params.idJson].verse;

    const englishVerses =
      English[this.props.navigation.state.params.idJson].verse;

    this.verses = arabicVerses.map((verse, index) => ({
      verse,
      index,
      english: englishVerses[index]
    }));

    this.state = {
      dataProvider: this.dataProvider.cloneWithRows(this.verses.slice(0, 10)),
      layoutProvider: new LayoutProvider(
        index => {
          return 0;
        },
        (type, dim) => {
          dim.width = width;
          dim.height = height;
        }
      )
    };
  }

  verses = [];

  dataProvider = new DataProvider((r1, r2) => {
    return r1.index !== r2.index;
  });

  renderItem = (type, item) => {
    return (
      <Surah
        verse={item.verse}
        // key={item.index}
        index={item.index}
        english={item.english}
      />
    );
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    this.setState(prevState => {
      const list = this.verses.slice(0, prevState.dataProvider._size + 10);
      const dataProvider = this.dataProvider.cloneWithRows(list);

      return {
        dataProvider
      };
    });
  };
  render() {
    return (
      <RecyclerListView
        onEndReached={this.loadData}
        onEndReachedThreshold={10}
        // initialOffset={10}
        itemAnimator={null}
        optimizeForInsertDeleteAnimations={true}
        canChangeSize={true}
        forceNonDeterministicRendering={true}
        layoutProvider={this.state.layoutProvider}
        dataProvider={this.state.dataProvider}
        rowRenderer={this.renderItem}
      />
    );
  }
}

export default withNavigation(Details);

const Surah = ({ verse, index, english }) => {
  return (
    <Container bgColor={index}>
      {/* arabic */}
      <Container.Arabic>
        <Container.Arabic.Text>{verse}</Container.Arabic.Text>
      </Container.Arabic>
      {/* english */}
      <Container.English>
        <Container.English.Text>
          {index + 1}.{"  "}
          {english}
        </Container.English.Text>
      </Container.English>
      {/* icons */}
      <Icons>
        <TouchableOpacity>
          <Icons.Icon
            // style={{ color: item.read ? "green" : "black" }}
            name="check-outline"
          />
        </TouchableOpacity>
        <TouchableOpacity
        // onPress={() => (item.like = !item.like)}
        >
          <Icons.Icon
            // style={{ color: item.like ? "red" : "black" }}
            name="heart"
          />
        </TouchableOpacity>
        <TouchableOpacity
        // onPress={() => (item.like = !item.like)}
        >
          <Icons.Icon
            // style={{ color: item.like ? "red" : "black" }}
            name="bookmark"
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Icons.Icon name="play-circle-outline" />
        </TouchableOpacity>
        <Icons.Share>
          <Icons.Icon name="share" />
        </Icons.Share>
      </Icons>
    </Container>
  );
};
