import React, { Component, useState } from "react";
import {
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  ScrollView,
  Animated,
  FlatList,
  View,
  Text,
  Platform,
  Alert,
  ImageBackground
} from "react-native";
import { withNavigation } from "react-navigation";
import { Container, List, TopShadow } from "./styles/QuranStyle";
import { DATA } from "../assets/QuranData";
import QuranContext from "../contexts/QuranProvider";
import Arabic from "../assets/Arabic";
import {
  RecyclerListView,
  DataProvider,
  LayoutProvider
} from "recyclerlistview";
import { BoxShadow } from "react-native-shadow";
const img = require("../assets/images/bismillah7.png");

let { width, height } = Dimensions.get("window");

const shadowOpt = {
  width: width,
  height: 150,
  color: "#000",
  border: 2,
  radius: 10,
  opacity: 0.2,
  x: 5,
  y: 3,
  style: { marginVertical: 5, style: { background: "red", side: "top" } }
};

interface Props {
  navigation: any;
}

class QuranList extends Component<Props, any> {
  constructor(props) {
    super(props);

    const arabicVerses =
      // Arabic[this.props.navigation.state.params.idJson].verse;

      (this.list = DATA.map((verse, index) => ({
        verse,
        index
      })));

    this.state = {
      dataProvider: this.dataProvider.cloneWithRows(this.list),
      layoutProvider: new LayoutProvider(
        index => {
          return 0;
        },
        (type, dim) => {
          dim.width = width;
          dim.height = 75;
          // dim.height = Platform.OS === "ios" ? 75 : 75;
        }
      )
    };
  }

  list = [];

  dataProvider = new DataProvider((r1, r2) => {
    return r1.index !== r2.index;
  });

  renderItem = (type, item) => {
    return (
      <Item
        navigation={this.props.navigation}
        item={item.verse}
        // key={item.index}
        index={item.index}
      />
    );
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    this.setState(prevState => {
      const list = this.list.slice(0, prevState.dataProvider._size + 30);
      const dataProvider = this.dataProvider.cloneWithRows(list);

      return {
        dataProvider
      };
    });
  };
  render() {
    return (
      <Container>
        <ImageBackground source={img} style={{ width: "100%", height: "100%" }}>
          <RecyclerListView
            // initialRenderIndex={113}
            // onEndReached={this.loadData}
            onEndReachedThreshold={0.1}
            // initialOffset={10}
            itemAnimator={null}
            // optimizeForInsertDeleteAnimations={true}
            // canChangeSize={true}
            // forceNonDeterministicRendering={true}
            layoutProvider={this.state.layoutProvider}
            dataProvider={this.state.dataProvider}
            rowRenderer={this.renderItem}
          />
        </ImageBackground>
      </Container>
    );
  }
}
export default withNavigation(QuranList);

const Item = ({ item, index, navigation }) => {
  // console.log(item);
  const [donwloaded, setDownloaded] = useState(true);
  return (
    <TouchableOpacity>
      {/* <BoxShadow setting={shadowOpt}> */}
      <TopShadow>
        <List
          onPress={() =>
            navigation.navigate("Detail", {
              idJson: item.id,
              title: Arabic[item.id].name
            })
          }
        >
          <List.Item.Left onPress={() => console.log("item.downloaded")}>
            <List.Item.Left.Left>
              <List.Item.Id>{index + 1}</List.Item.Id>
            </List.Item.Left.Left>
            <List.Item.Left.Right>
              <List.Item.englishName>{item.name_simple}</List.Item.englishName>
              <List.Item.englishTraslation>
                {item.translated_name.name}({item.verses_count})
              </List.Item.englishTraslation>
            </List.Item.Left.Right>
          </List.Item.Left>
          {/* the right */}
          <List.Item.Right>
            <List.Item.arabicName>{item.name_arabic}</List.Item.arabicName>
            <TouchableOpacity onPress={() => console.log("item.downloaded")}>
              <List.Item.Icon
                name={item.downloaded ? "done" : "file-download"}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (donwloaded) {
                  Alert.alert("Please Download First");
                }
              }}
            >
              {/* <List.Item.Icon name={"play-circle-outline"} /> */}
            </TouchableOpacity>
          </List.Item.Right>
        </List>
      </TopShadow>
      {/* </BoxShadow> */}
    </TouchableOpacity>
  );
};
