import surah1 from "./surah_1";
import surah2 from "./surah_2";

export default { surah1, surah2 };
