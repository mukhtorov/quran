import React from "react";
import { Text } from "react-native";
import { withNavigation } from "react-navigation";
import { Container, OrderSelector } from "./styles/BookmarkStyle";
import QuranContext from "../contexts/QuranProvider";

const Bookmark = ({ navigation }) => {
  return (
    <QuranContext.Consumer>
      {context => (
        <Container>
          <Text>Bookmark screen </Text>
        </Container>
      )}
    </QuranContext.Consumer>
  );
};

export default withNavigation(Bookmark);
