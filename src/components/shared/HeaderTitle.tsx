import React from "react";
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";
import { Platform } from "react-native";

const Logo = styled.Text`
  margin-left: 16;
  color: #fff;
  font-size: 20;
  display: flex;
  align-items: ${Platform.OS === "ios" ? "flex-start" : "flex-start"};
`;

const headerTitle = ({ navigation }) => {
  return (
    <Logo
      onPress={() => {
        navigation.navigate("Home", { isOpenedSearch: false });
      }}
    >
      Quran
    </Logo>
  );
};

export default withNavigation(headerTitle);
