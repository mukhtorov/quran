import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import styled from "styled-components/native";
import { withNavigation } from "react-navigation";

const MenuIcon = styled(Icon)`
  margin-right: 16;
`;

const HeaderRight = ({ navigation }) => {
  return (
    <MenuIcon
      name="bars"
      size={24}
      color="#fff"
      onPress={() => {
        console.log("clicked");
        navigation.toggleDrawer();
      }}
    />
  );
};

export default withNavigation(HeaderRight);
