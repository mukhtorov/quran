import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import styled from "styled-components";
import { Platform } from "react-native";

const Tabs = styled.View`
  height: ${Platform.OS === "ios" ? "74" : "68"};
  flex-direction: row;
  border-top-width: 1;
  border-top-color: #7b88a6;
  /* box-shadow: black; */
  /* scrollbar-darkshadow-color: blanchedalmond; */
  /* scrollbar-shadow-color: whitesmoke; */
  padding-bottom: ${Platform.OS === "ios" ? "30" : "10"};
  background-color: #9ab9f2;
`;

const Tab = styled.TouchableOpacity`
  flex: 1;
  align-items: center;
  justify-content: center;
  /* box-shadow: black; */
`;
``;
interface Props {
  scrollValue?: any;
  style?: any;
  goToPage?: any;
  activeTab?: any;
  tabs?: any;
}

class TabBar extends React.Component<Props> {
  // icons = [];
  // _listener: any;

  constructor(props) {
    super(props);
    // this.icons = [];
  }

  componentDidMount() {
    // this._listener = this.props.scrollValue.addListener(
    //   this.setAnimationValue.bind(this)
    // );
  }
  // iconColor(progress) {
  //   const red = 59 + (204 - 59) * progress;
  //   const green = 89 + (204 - 89) * progress;
  //   const blue = 152 + (204 - 152) * progress;
  //   return `rgb(${red}, ${green}, ${blue})`;
  // }
  isOpened = false;
  render() {
    return (
      <Tabs style={this.props.style}>
        {this.props.tabs.map((tab, i, isOpened) => {
          return (
            <Tab
              style={{ shadow: "red" }}
              key={tab}
              onPress={() => this.props.goToPage(i, isOpened)}
            >
              <Icon
                name={tab}
                size={24}
                color={
                  this.props.activeTab === i ? "#oeobof" : "rgb(204,204,204)"
                }
              />
            </Tab>
          );
        })}
      </Tabs>
    );
  }
}

export default TabBar;
