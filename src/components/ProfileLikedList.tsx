import React from "react";
import { Text } from "react-native";
import { withNavigation } from "react-navigation";
import MasonryList from "react-native-masonry-list";
import { Container } from "./styles/ProfileStyle";
import ProfileInfo from "./shared/ProfileInfo";
import QuranContext from "../contexts/QuranProvider";

const ProfileLikedList = ({ navigation }) => {
  return (
    <QuranContext.Consumer>
      {context => (
        <Container>
          <Text>Profile liked list screen </Text>
        </Container>
      )}
    </QuranContext.Consumer>
  );
};

export default withNavigation(ProfileLikedList);
