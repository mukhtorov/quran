import React from "react";
import { Text } from "react-native";
import { Container } from "./styles/YouTubeStyle";
import QuranContext from "../contexts/QuranProvider";
// import YoutubePlayer from "react-native-youtube-sdk";
import YouTube from "react-native-youtube";

const YouTubeDisplay = () => {
  const youTubeRef = React.createRef();
  return (
    <QuranContext.Consumer>
      {context => (
        <Container>
          {/* <YouTube
            ref={youTubeRef}
            videoId="QXSyqkCG0VM" // The YouTube video ID
            apiKey="AIzaSyDAME3gkLBXFOuHABmpj4FlKloiW1JDTVc"
            play={true} // control playback of video with true/false
            fullscreen={true} // control whether the video should play in fullscreen or inline
            loop={true} // control whether the video should loop when ended
            // onReady={e => this.setState({ isReady: true })}
            // onChangeState={e => this.setState({ status: e.state })}
            // onChangeQuality={e => this.setState({ quality: e.quality })}
            // onError={e => this.setState({ error: e.error })}
            style={{ alignSelf: "stretch", height: 300 }}
          /> */}
        </Container>
      )}
    </QuranContext.Consumer>
  );
};

export default YouTubeDisplay;
