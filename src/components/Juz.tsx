import React from "react";
import { Text } from "react-native";
import { Container } from "./styles/JuzStyle";
import { withNavigation } from "react-navigation";
import QuranContext from "../contexts/QuranProvider";

interface Props {
  navigation: any;
}
class Juz extends React.Component<Props, any> {
  render() {
    // console.log("props", this.props.navigation);
    return (
      <QuranContext.Consumer>
        {context => (
          <Container>
            <Text>Juz screen </Text>
          </Container>
        )}
      </QuranContext.Consumer>
    );
  }
}

export default withNavigation(Juz);
