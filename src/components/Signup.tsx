import React from "react";
import { Text } from "react-native";
import { Container } from "./styles/SignupStyle";
import QuranContext from "../contexts/QuranProvider";

const Signup = () => {
  return (
    <QuranContext.Consumer>
      {context => (
        <Container>
          <Text>Sign up screen </Text>
        </Container>
      )}
    </QuranContext.Consumer>
  );
};

export default Signup;
