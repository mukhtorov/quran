import React, { useState } from "react";
import { Container, Menu } from "./styles/DrawerStyle";
import {
  DrawerInput,
  LoginButton,
  ButtonGroup,
  LoginForm
} from "./styles/LoginFormStyle";
import QuranContext from "../contexts/QuranProvider";

const Drawer = ({ navigation }) => {
  const [showLoginForm, setShowLoginForm] = useState(false);

  return (
    <Container>
      <Menu>
        <Menu.Item
          // style={{ padding: "20" }}
          onPress={() => {
            setShowLoginForm(false);
            navigation.navigate("Signup");
          }}
        >
          <Menu.Item.Text>무료 계정 만들기</Menu.Item.Text>
        </Menu.Item>
        <Menu.Item disabled>
          <Menu.Item.Text>또는</Menu.Item.Text>
        </Menu.Item>
        <Menu.Item
          onPress={() => {
            setShowLoginForm(!showLoginForm);
          }}
        >
          <Menu.Item.Text>로그인</Menu.Item.Text>
        </Menu.Item>
      </Menu>
      {showLoginForm && (
        <LoginForm show={showLoginForm}>
          <DrawerInput placeholder="이메일" />
          <DrawerInput placeholder="비밀번호" />
          <ButtonGroup>
            <LoginButton
              color="yellow"
              onPress={() => console.log("button clicked")}
            >
              <LoginButton.Text>로그인</LoginButton.Text>
            </LoginButton>
          </ButtonGroup>
          <ButtonGroup>
            <LoginButton
              size="small"
              color="google"
              onPress={() => console.log("button clicked")}
            >
              <LoginButton.Icon name="google" color="white" size="16" />
              <LoginButton.Text color="white">Google</LoginButton.Text>
            </LoginButton>
            <LoginButton
              size="small"
              color="facebook"
              onPress={() => console.log("button clicked")}
            >
              <LoginButton.Icon name="facebook" color="white" size="16" />
              <LoginButton.Text color="white">Facebook</LoginButton.Text>
            </LoginButton>
          </ButtonGroup>
        </LoginForm>
      )}
    </Container>
  );
};

export default Drawer;
