import styled from "styled-components/native";
import Icon from "react-native-vector-icons/FontAwesome";

export const Container = styled.View`
  flex: 1;
  display: flex;
  flex-direction: column;
  background-color: #545454;
`;

export const OrderSelector = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-bottom-width: 1;
  border-bottom-color: #eee;
`;

OrderSelector.Title = styled.Text`
  margin-top: 16;
  margin-bottom: 8;
  font-size: 14;
`;

OrderSelector.List = styled.View`
  display: flex;
  flex-direction: row;
`;

OrderSelector.List.Item = styled.TouchableOpacity`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 48;
`;

OrderSelector.List.Item.Icon = styled(Icon)``;

export const ListItem = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;
  margin-top: 8;
  margin-left: 16;
  margin-right: 16;
  margin-bottom: 8;
`;

ListItem.Thumbnail = styled.View`
  margin-right: 8;
`;

ListItem.Thumbnail.Image = styled.Image`
  width: 100;
  height: 100;
`;

ListItem.Description = styled.View`
  flex: 1;
`;

ListItem.Description.Tags = styled.ScrollView`
  display: flex;
  flex-direction: row;
`;

ListItem.Description.Tags.Item = styled.Text`
  background-color: #fffde7;
  padding-top: 8;
  padding-left: 16;
  padding-right: 16;
  padding-bottom: 8;
  margin-top: 8;
  margin-bottom: 8;
  margin-left: 4;
  margin-right: 4;
  border-radius: 8;
  color: #000;
`;

ListItem.Description.Status = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 8;
  margin-left: 8;
`;

ListItem.Description.Status.Group = styled.View`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

ListItem.Description.Status.Group.Icon = styled(Icon)``;

ListItem.Description.Status.Group.Text = styled.Text`
  margin-left: 8;
  font-size: 14;
  color: #000;
`;
