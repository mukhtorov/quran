import styled from "styled-components/native";

export const Container = styled.ScrollView`
  flex: 1;
  display: flex;
  flex-direction: column;
  background-color: #1c1b1b;
`;

export const BodyView = styled.View`
  display: flex;
  background-color: white;
  margin-left: 16;
  margin-right: 16;
  margin-bottom: 32;
  padding-top: 16;
  padding-bottom: 32;
  border-radius: 8;
`;

export const DrawerText = styled.Text`
  color: #000;
  font-size: 14;
  margin-top: 16;
  margin-left: 16;
  margin-right: 16;
`;
