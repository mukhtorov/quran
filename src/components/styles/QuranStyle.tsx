import styled from "styled-components/native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Dimensions } from "react-native";
let { width, height } = Dimensions.get("window");

export const Container = styled.View`
  /* width: 100%; */
  display: flex;
  flex: 1;
  margin: 0;
  background-color: #8fabeb;
`;

export const List = styled.TouchableOpacity`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 10px;
  background: #adcaff;
  border-radius: 10px;
  shadow-opacity: 0.6;
  shadow-radius: 5px;
  shadow-color: black;
  shadow-offset: 4px 4px;
  /* margin-right: 16px;
  margin-left: 16px; */
`;

export const TopShadow = styled.TouchableOpacity`
  /* width: ${width}; */
  /* display: flex; */
  /* flex-direction: row; */
  /* justify-content: space-between; */
  /* background: red; */
  /* background: #9ab9f2; */
  /* border-radius: 2px; */
  shadow-opacity: 1;
  shadow-radius: 5px;
  shadow-color: white;
  shadow-offset: -4px -4px;
  margin-right: 16px;
  margin-left: 16px; 

`;

List.Item = styled.View`
  color: white;
`;
List.Item.Text = styled.Text`
  color: white;
`;
// left
List.Item.Left = styled.View`
  display: flex;
  flex-direction: row;
  color: red;
`;
// left left
List.Item.Left.Left = styled.View`
  display: flex;
  width: 55px;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding-right: 8px;
  color: red;
`;
// left right
List.Item.Left.Right = styled.View`
  display: flex;
  flex-direction: center;
  /* align-items: center; */
  flex-direction: column;
  color: red;
`;

// right
List.Item.Right = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  color: white;
`;

// ID
List.Item.Id = styled.Text`
  display: flex;
  /* flex-direction: center; */
  align-self: center;
  justify-content: center;
  color: #69696b;
  font-size: 21;
  /* padding-right: 10px; */
`;
// english name
List.Item.englishName = styled.Text`
  /* font-family: Arial; */
  font-size: 18px;
  color: #0e0b0f;
`;
// english translation
List.Item.englishTraslation = styled.Text`
  color: #40405d;
`;
// arabic name
List.Item.arabicName = styled.Text`
  /* font-family: Arial; */
  font-size: 16px;
  color: #0e0b0f;
`;
// download icon
List.Item.Icon = styled(Icon)`
  /* color: #0e0b0f; */
  font-size: 24;
  padding-left: 10px;
`;
