import styled from "styled-components";

export const Container = styled.ScrollView`
  flex: 1;
`;

export const Photo = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 48;
  padding-bottom: 32;
`;

Photo.Image = styled.Image`
  width: 64;
  height: 64;
`;

export const TextGroup = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  border-bottom-width: 1;
  border-bottom-color: #eee;
  margin-top: 16;
  margin-left: 16;
  margin-right: 16;
  height: 48;
`;

TextGroup.Label = styled.Text`
  width: 80;
  color: #808080;
  font-size: 14;
`;

TextGroup.TextInput = styled.TextInput`
  flex: 1;
  height: 48;
  font-size: 14;
`;

TextGroup.Text = styled.Text`
  flex: 1;
  font-size: 14;
`;

export const Submit = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 32;
  margin-left: 16;
  margin-right: 16;
  margin-bottom: 16;
  background-color: #fdd835;
  border-width: 1;
  border-color: #f9a825;
  border-radius: 8;
  height: 48;
`;

Submit.Text = styled.Text`
  color: #000;
`;
