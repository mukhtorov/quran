import styled from "styled-components/native";
import Icon from "react-native-vector-icons/FontAwesome";
export const Container = styled.View`
  display: flex;
  flex-direction: column;
  height: 100%;
  background-color: #545454;
`;
