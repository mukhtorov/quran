import styled from "styled-components/native";
import Icon from "react-native-vector-icons/FontAwesome";

export const Container = styled.View`
  flex: 1;
  display: flex;
  flex-direction: column;
  margin-bottom: 21px;
`;

export const ProcessInput = styled.TextInput`
  background-color: rgba(0, 0, 0, 0.1);
  margin-top: 16;
  margin-left: 8;
  margin-right: 8;
  padding-left: 16;
  padding-right: 16;
  border: 1px solid #3650f7;
  border-radius: 8;
  height: 48;
`;

const drawerButtonSizes = {
  small: 40,
  medium: 48
};

export const ProcessButton = styled.TouchableOpacity`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  color: white;
  background-color: #448aff;
  height: ${props => drawerButtonSizes[props.size || "medium"]};
  margin-top: 16;
  margin-left: 8;
  margin-right: 8;
  border-radius: 8;
`;
ProcessButton.Text = styled.Text`
  font-size: 16;
  color: white;
`;
export const ProcessWarning = styled.View`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  color: white;
  background-color: rgba(0, 0, 0, 0.8);
  height: ${props => drawerButtonSizes[props.size || "medium"]};
  margin-top: 16;
  margin-left: 8;
  margin-right: 8;
  border-radius: 8;
`;
ProcessWarning.Text = styled.Text`
  font-size: 16;
  color: white;
`;
export const ProcesImageView = styled.View`
  top: 30;
  margin-bottom: 31;
  margin-right: 55;
  margin-left: 56;
  /* display: flex;
  flex-direction: row; */
`;

export const ProcesImage = styled.Image`
  width: 100%;
  height: 306;
  border-radius: 4;
`;

export const ProcessBar = styled.View`
  display: flex;
  flex-direction: row;
  margin-right: 17;
  margin-left: 17;
  margin-top: 17;
`;
ProcessBar.Icon = styled(Icon)``;
ProcessBar.View = styled.Text`
  margin-left: auto;
  font-size: 24;
`;
