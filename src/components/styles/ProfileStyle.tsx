import styled from "styled-components";
import Icon from "react-native-vector-icons/FontAwesome";

export const Container = styled.View`
  flex: 1;
  display: flex;
  flex-direction: column;
  background-color: #fff;
`;

export const Content = styled.View`
  padding-bottom: 16;
  border-bottom-width: 1;
  border-bottom-color: #eee;
`;

export const ProfileHeader = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 16;
  margin-left: 16;
  margin-right: 16;
  margin-bottom: 16;
`;

ProfileHeader.Photo = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 48;
  height: 48;
  margin-right: 8;
`;

ProfileHeader.Photo.Icon = styled(Icon)``;

ProfileHeader.Photo.Image = styled.Image`
  width: 48;
  height: 48;
`;

ProfileHeader.Text = styled.Text`
  flex: 1;
  font-size: 14;
`;

export const ProfileMenu = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-left: 16;
  margin-right: 16;
  padding-left: 8;
  padding-right: 8;
  background-color: #eee;
  border-radius: 8;
`;

ProfileMenu.Item = styled.TouchableOpacity`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 8;
  margin-bottom: 8;
  margin-left: 8;
  margin-right: 8;
  padding-top: 8;
  padding-bottom: 8;
  ${props => (props.isActive ? "background-color: #fff;" : "")}
`;

ProfileMenu.Item.Icon = styled(Icon)``;
