import styled from "styled-components/native";
import Icon from "react-native-vector-icons/FontAwesome";

export const LoginForm = styled.View`
  border-top-color: #424242;
  border-top-width: 1;
  display: flex;
  margin-bottom: 16;
`;

export const DrawerInput = styled.TextInput`
  background-color: #fff;
  margin-top: 16;
  margin-left: 16;
  margin-right: 16;
  padding-left: 16;
  padding-right: 16;
  border: 1px solid #3650f7;
  border-radius: 8;
  height: 48;
`;

export const ButtonGroup = styled.View`
  display: flex;
  flex-direction: row;
  margin-left: 12;
  margin-right: 12;
`;

const drawerButtonColors = {
  google: {
    color: "#fff",
    backgroundColor: "#388fe0"
  },
  facebook: {
    color: "#fff",
    backgroundColor: "#004f99"
  },
  yellow: {
    color: "#000",
    backgroundColor: "#FDD835"
  }
};

const drawerButtonSizes = {
  small: 40,
  medium: 48
};

export const LoginButton = styled.TouchableOpacity`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  color: ${props => drawerButtonColors[props.color || "yellow"].color};
  background-color: ${props =>
    drawerButtonColors[props.color || "yellow"].backgroundColor};
  height: ${props => drawerButtonSizes[props.size || "medium"]};
  margin-top: 16;
  margin-left: 4;
  margin-right: 4;
  border-radius: 8;
`;

LoginButton.Icon = styled(Icon)`
  font-size: 16;
  margin-right: 16;
`;

LoginButton.Text = styled.Text`
  font-size: 16;
  color: ${props => props.color || "black"};
`;
