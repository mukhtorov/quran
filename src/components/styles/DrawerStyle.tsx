import styled from "styled-components/native";
import { Platform } from "react-native";

export const Container = styled.ScrollView`
  flex: 1;
  display: flex;
  flex-direction: column;
  margin-top: ${Platform.OS === "ios" ? "50px" : "10"};
`;

export const Menu = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  margin-top: 4;
  margin-bottom: 4;
`;

Menu.Item = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 48;
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  padding-left: 16;
`;

Menu.Item.Text = styled.Text`
  flex: 1;
  color: #fff;
  font-size: 14px;
`;
