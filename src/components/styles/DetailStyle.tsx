import styled from "styled-components/native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { TouchableOpacity } from "react-native";

const bgColor = {
  backgroundColor: "#f5f5f5",
  color: "#fff"
};

export const BodyContainer = styled.View`
  flex: 1;
  display: flex;
  flex-direction: column;
  background-color: white;
  margin-bottom: 20px;
  width: 100%;
`;
export const Juz = styled.Text`
  display: flex;
  background-color: black;
  color: white;
  padding: 16px;
  margin-bottom: 30px;
`;

export const Container = styled.View`
  width: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  padding-top: 16px;
  padding-bottom: 16px;
  /* padding-left: 16px;
  padding-left: 16px; */
  background-color: ${props =>
    props.bgColor % 2 === 0 ? bgColor.backgroundColor : bgColor.color};
`;

Container.Arabic = styled.View`
  display: flex;
  flex-direction: row-reverse;
  /* align: right; */
  /* justify-content: ; */
  padding-right: 27px;
  padding-left: 16px;
`;

Container.English = styled.View`
  display: flex;
  flex-direction: row;
  /* justify-content: space-between; */
  padding-right: 16px;
  padding-left: 16px;
  padding-top: 6px;
`;
Container.Arabic.Text = styled.Text`
  display: flex;
  flex-direction: row;
  color: black;
  font-size: 20;
  /* margin-left: 36px; */
`;
Container.English.Text = styled.Text`
  display: flex;
  flex-direction: row;
  color: #3d3d3d;
  font-size: 18;
`;

Container.English.Number = styled.Text`
  display: flex;
  flex-direction: row;
  padding-right: 16px;
  padding-left: 16px;
  color: black;
  font-size: 18;
`;

export const Icons = styled.View`
  display: flex;
  flex-direction: row;
  margin-top: 8;
  margin-left: 16;
`;
Icons.Icon = styled(Icon)`
  font-size: 20;
  padding-right: 16;
`;
Icons.Share = styled.TouchableOpacity`
  margin-left: auto;
  padding-right: 20;
`;

export const Title = styled.View`
  display: flex;
  width: 100%;
`;

Title.Image = styled.Image`
  align-content: center;
  align-self: center;
  justify-content: center;
  resize-mode: stretch;
  height: 65px;
  width: 80%;
`;
